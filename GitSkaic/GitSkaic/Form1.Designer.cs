﻿namespace GitSkaic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLygybe = new System.Windows.Forms.Button();
            this.txtPirmasDemuo = new System.Windows.Forms.TextBox();
            this.btnPlius = new System.Windows.Forms.Button();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btnDaugyba = new System.Windows.Forms.Button();
            this.btnDalyba = new System.Windows.Forms.Button();
            this.lblSimbolis = new System.Windows.Forms.Label();
            this.txtAntrasDemuo = new System.Windows.Forms.TextBox();
            this.lblLygybe = new System.Windows.Forms.Label();
            this.txtRezult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnLygybe
            // 
            this.btnLygybe.Location = new System.Drawing.Point(26, 90);
            this.btnLygybe.Name = "btnLygybe";
            this.btnLygybe.Size = new System.Drawing.Size(452, 23);
            this.btnLygybe.TabIndex = 0;
            this.btnLygybe.Text = "=";
            this.btnLygybe.UseVisualStyleBackColor = true;
            this.btnLygybe.Click += new System.EventHandler(this.btnLygybe_Click);
            // 
            // txtPirmasDemuo
            // 
            this.txtPirmasDemuo.Location = new System.Drawing.Point(26, 23);
            this.txtPirmasDemuo.Name = "txtPirmasDemuo";
            this.txtPirmasDemuo.Size = new System.Drawing.Size(100, 20);
            this.txtPirmasDemuo.TabIndex = 1;
            // 
            // btnPlius
            // 
            this.btnPlius.Location = new System.Drawing.Point(26, 49);
            this.btnPlius.Name = "btnPlius";
            this.btnPlius.Size = new System.Drawing.Size(75, 23);
            this.btnPlius.TabIndex = 2;
            this.btnPlius.Text = "+";
            this.btnPlius.UseVisualStyleBackColor = true;
            this.btnPlius.Click += new System.EventHandler(this.btnPlius_Click);
            // 
            // btnMinus
            // 
            this.btnMinus.Location = new System.Drawing.Point(150, 49);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(75, 23);
            this.btnMinus.TabIndex = 3;
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = true;
            this.btnMinus.Click += new System.EventHandler(this.btnMinus_Click);
            // 
            // btnDaugyba
            // 
            this.btnDaugyba.Location = new System.Drawing.Point(283, 49);
            this.btnDaugyba.Name = "btnDaugyba";
            this.btnDaugyba.Size = new System.Drawing.Size(75, 23);
            this.btnDaugyba.TabIndex = 4;
            this.btnDaugyba.Text = "*";
            this.btnDaugyba.UseVisualStyleBackColor = true;
            this.btnDaugyba.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnDalyba
            // 
            this.btnDalyba.Location = new System.Drawing.Point(403, 49);
            this.btnDalyba.Name = "btnDalyba";
            this.btnDalyba.Size = new System.Drawing.Size(75, 23);
            this.btnDalyba.TabIndex = 5;
            this.btnDalyba.Text = "/";
            this.btnDalyba.UseVisualStyleBackColor = true;
            this.btnDalyba.Click += new System.EventHandler(this.btnDalyba_Click);
            // 
            // lblSimbolis
            // 
            this.lblSimbolis.AutoSize = true;
            this.lblSimbolis.Location = new System.Drawing.Point(163, 26);
            this.lblSimbolis.Name = "lblSimbolis";
            this.lblSimbolis.Size = new System.Drawing.Size(13, 13);
            this.lblSimbolis.TabIndex = 6;
            this.lblSimbolis.Text = "_";
            // 
            // txtAntrasDemuo
            // 
            this.txtAntrasDemuo.Location = new System.Drawing.Point(202, 23);
            this.txtAntrasDemuo.Name = "txtAntrasDemuo";
            this.txtAntrasDemuo.Size = new System.Drawing.Size(100, 20);
            this.txtAntrasDemuo.TabIndex = 7;
            // 
            // lblLygybe
            // 
            this.lblLygybe.AutoSize = true;
            this.lblLygybe.Location = new System.Drawing.Point(333, 26);
            this.lblLygybe.Name = "lblLygybe";
            this.lblLygybe.Size = new System.Drawing.Size(13, 13);
            this.lblLygybe.TabIndex = 8;
            this.lblLygybe.Text = "=";
            // 
            // txtRezult
            // 
            this.txtRezult.Location = new System.Drawing.Point(378, 23);
            this.txtRezult.Name = "txtRezult";
            this.txtRezult.Size = new System.Drawing.Size(100, 20);
            this.txtRezult.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 167);
            this.Controls.Add(this.txtRezult);
            this.Controls.Add(this.lblLygybe);
            this.Controls.Add(this.txtAntrasDemuo);
            this.Controls.Add(this.lblSimbolis);
            this.Controls.Add(this.btnDalyba);
            this.Controls.Add(this.btnDaugyba);
            this.Controls.Add(this.btnMinus);
            this.Controls.Add(this.btnPlius);
            this.Controls.Add(this.txtPirmasDemuo);
            this.Controls.Add(this.btnLygybe);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLygybe;
        private System.Windows.Forms.TextBox txtPirmasDemuo;
        private System.Windows.Forms.Button btnPlius;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.Button btnDaugyba;
        private System.Windows.Forms.Button btnDalyba;
        private System.Windows.Forms.Label lblSimbolis;
        private System.Windows.Forms.TextBox txtAntrasDemuo;
        private System.Windows.Forms.Label lblLygybe;
        private System.Windows.Forms.TextBox txtRezult;
    }
}

